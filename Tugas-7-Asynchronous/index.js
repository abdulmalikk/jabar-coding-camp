// Abdul Malik
// Tugas 7 JCC - Asynchronous pada javascript


// ========== jawaban soal no. 1 ==========
const readBooks = require('./callback.js');

const books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

console.log('^^^^^^^^^^ No. 1 ^^^^^^^^^^');

readBooks(10000, books[0], function(sisaWaktu) {
  readBooks(sisaWaktu, books[1], function(sisaWaktu) {
    readBooks(sisaWaktu, books[2], function(sisaWaktu) {
      readBooks(sisaWaktu, books[3], function(sisaWaktu) {
      });
    });
  });
});
// ========== jawaban soal no. 1 ==========
