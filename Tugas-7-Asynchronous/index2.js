// Abdul Malik
// Tugas 7 JCC - Asynchronous pada javascript


// ========== jawaban soal no. 2 ==========
const readBooksPromise = require('./promise.js');

const books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 },
];

console.log('^^^^^^^^^^ No. 2 ^^^^^^^^^^');

readBooksPromise(10000, books[0])
  .then(function(sisaWaktu) {
    readBooksPromise(sisaWaktu, books[1])
      .then(function(sisaWaktu) {
        readBooksPromise(sisaWaktu, books[2])
          .then(function(sisaWaktu) {
            readBooksPromise(sisaWaktu, books[3])
              .then(function(sisaWaktu) {
              });
          });
      });
  });
// ========== jawaban soal no. 2 ==========
