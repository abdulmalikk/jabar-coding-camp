// Abdul Malik
// Tugas 4 JCC - pengkondisian dan loop javascript


// ========== soal 1 ==========
var nilai = 70;
var indeks;

// pengkondisian if else yang hasilnya akan diisikan ke variabel indeks
if (nilai >= 85) {
  indeks = 'A';
} else if (nilai >= 75 && nilai < 85) {
  indeks = 'B';
} else if (nilai >= 65 && nilai < 875) {
  indeks = 'C';
} else if (nilai >= 55 && nilai < 65) {
  indeks = 'D';
} else {
  indeks = 'E';
}

// variabel hasil1 untuk menampung hasil akhir jawaban soal no. 1
var hasil1 = 'Indeks dari nilai terbeut adalah : ' + indeks;
console.log('^^^^^^^^^^ No. 1 ^^^^^^^^^^');
console.log(hasil1);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 1 ==========


// ========== soal 2 ==========
var tanggal = 30;
var bulan = 10;
var tahun = 2002;

// switch case yang menentukan nama bulan dari setiap angka 1 - 12, dan akan menghasilkan string "bulan tidak diketahui" jika selain angka tersebut
switch (bulan) {
  case 1:
    bulan = 'Januari';
    break;
  case 2:
    bulan = 'Februari';
    break;
  case 3:
    bulan = 'Maret';
    break;
  case 4:
    bulan = 'April';
    break;
  case 5:
    bulan = 'Mei';
    break;
  case 6:
    bulan = 'Juni';
    break;
  case 7:
    bulan = 'Juli';
    break;
  case 8:
    bulan = 'Agustus';
    break;
  case 9:
    bulan = 'September';
    break;
  case 10:
    bulan = 'Oktober';
    break;
  case 11:
    bulan = 'November';
    break;
  case 12:
    bulan = 'Desember';
    break;
  default:
    bulan = 'Bulan tidak diketahui';
    break;
}

// variabel hasil2 untuk menampung hasil akhir jawaban soal no. 2
var hasil2 = tanggal + ' ' + bulan + ' ' + tahun;
console.log('^^^^^^^^^^ No. 2 ^^^^^^^^^^');
console.log(hasil2);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 2 ==========


// ========== soal 3 ==========
var n = 7;
// variabel hasil3 berisi string kosong, yang nantinya akan diisi string untuk jawaban soal no. 3
var hasil3 = '';

// perulangan for yang digunakan untuk perulangan setiap baris segitiga, yang jumlah barisnya ditentukan sesuai variabel n
for (var i = 0; i < n; i++) {
  // perulangan for yang digunakan untuk perulangan # di masing-masing baris, jumlah # ditentukan sesuai nomor urutan baris
  for (var z = 0; z <= i; z++) {
    hasil3 += '#';
  }
  hasil3 += '\n';
}
console.log('^^^^^^^^^^ No. 3 ^^^^^^^^^^');
console.log(hasil3);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 3 ==========


// ========== soal 4 ==========
var m = 10;
// variabel hasil4 berisi string kosong, yang nantinya akan diisi string untuk jawaban soal no. 4
var hasil4 = '';
// variabel reset berisi int 1, variabel ini nantinya hanya akan berisi 1 / 2 / 3
var reset = 1;
// variabel suka ini akan diisi string tergantung ada di angka berapa si variabel reset
var suka;

for (var i = 1; i <= m; i++) {
  // variabel batasKelipatan berisi string kosong, nantinya akan diisi =, untuk batas ketika melewati 3 kelipatan perulangan. dan jumlah = akan terus bertambah 3 di setiap 3 kelipatan perulangan tersebut
  var batasKelipatan = '';

  // switch case yang mengisi variabel suka berdasarkan angka reset sekarang
  switch (reset) {
    case 1:
      suka = 'programming';
      break;
    case 2:
      suka = 'Javascript';
      break;
    case 3:
      suka = 'VueJS';
      break;
  }
  // variabel hasil4 diisi
  hasil4 += i + ' - I love ' + suka + '\n';
  // variabel reset ditambah 1
  reset++;

  // pengkondisian ini akan memeriksa, apakah index perulangan yang sekarang jika dibagi 3 akan menghasilkan angka 0? jika iya berarti index yang sedang dijalani adalah angka kelipatan 3. nah fungsi dari pengkondisian ini adalah untuk menyisipkan variabel batasKelipatan
  if (i % 3 == 0) {
    // perulangan for disini berfungsi untuk menentukan berapa jumlah =
    for (var z = 1; z <= i; z++) {
      batasKelipatan += '=';
    }
    // variabel hasil4 diisi batasKelipatan
    hasil4 += batasKelipatan + '\n';
    // variabel reset diisi ulang menjadi int 1
    reset = 1;
  }
}
console.log('^^^^^^^^^^ No. 4 ^^^^^^^^^^');
console.log(hasil4);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 4 ==========
