// Abdul Malik
// Tugas 5 JCC - array, function, object pada javascript


// ========== soal 1 ==========
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
var hasil1 = '';

// pertama gunakan method sort() pada variabel daftarHewan untuk mengubah urutan elemennya
daftarHewan.sort();
// perulangan for untuk memasukan masing-masing elemen dari daftarHewan ke string hasil1
for (var i = 0; i < daftarHewan.length; i++) {
  hasil1 += daftarHewan[i] + '\n';
}
console.log('^^^^^^^^^^ No. 1 ^^^^^^^^^^');
console.log(hasil1);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 1 ==========


// ========== soal 2 ==========
function introduce(data) {
  // buat beberapa variabel untuk menampung value dari masing-masing key object dari parameter
  var nama = data.name;
  var umur = data.age;
  var alamat = data.address;
  var hobi = data.hobby;
  // return hasil dengan menggabungkan beberapa string dan variabel-variabel data yang sudah dibuat
  return 'Nama saya ' + nama + ', umur saya ' + umur + ' tahun, alamat saya di ' + alamat + ', dan saya punya hobby yaitu ' + hobi;
}
// variabel object yang berisi data, untuk dijadikan sebagai parameter pada function introduce
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

var perkenalan = introduce(data);
console.log('^^^^^^^^^^ No. 2 ^^^^^^^^^^');
console.log(perkenalan);    // Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 2 ==========


// ========== soal 3 ==========
function hitung_huruf_vokal(teks) {
  // membuat variabel vokal yang berfungsi untuk menampung semua huruf vokal
  var vokal = 'aiueo';
  // variabel hitungVokal, berisi int 0, berfungsi untuk melakukan counter jumlah huruf vokal yang ada
  var hitungVokal = 0;
  for (var i = 0; i < teks.length; i++) {
    // pengkondisian untuk menentukan apakah ada huruf dari parameter teks yang sama dengan huruf di variabel vokal
    if (vokal.indexOf(teks[i].toLowerCase()) != -1) {
      // dan jika ada, maka variabel hitungVokal ditambah valuenya
      hitungVokal++;
    }
  }
  // mengembalikan nilai dari hitungVokal
  return hitungVokal;
}
var hitung_1 = hitung_huruf_vokal('Abdul');
console.log('^^^^^^^^^^ No. 3 ^^^^^^^^^^');
console.log(hitung_1);    // 2
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 3 ==========


// ========== soal 4 ==========
function hitung(angka) {
  return (angka - 1) * 2;
}
console.log('^^^^^^^^^^ No. 4 ^^^^^^^^^^');
console.log(hitung(0));   // -2
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 4 ==========
