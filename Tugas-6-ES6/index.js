// Abdul Malik
// Tugas 6 JCC - ES6 pada javascript


// ========== soal 1 ==========
const luasKelilingPersegiPanjang = (p, l) => {
  const luas = p * l;
  const keliling = 2 * (p + l);
  return 'Luas persegi panjang : ' + luas + ', Keliling persegi panjang : ' + keliling;
}

//Driver Code
console.log('^^^^^^^^^^ No. 1 ^^^^^^^^^^');
console.log(luasKelilingPersegiPanjang(3, 8));
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 1 ==========


// ========== soal 2 ==========
const literal = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => console.log(firstName + ' ' + lastName),
  }
}

//Driver Code 
console.log('^^^^^^^^^^ No. 2 ^^^^^^^^^^');
literal('William', 'Imoh').fullName();
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 2 ==========


// ========== soal 3 ==========
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject;

//Driver Code
console.log('^^^^^^^^^^ No. 3 ^^^^^^^^^^');
console.log(firstName, lastName, address, hobby);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 3 ==========


// ========== soal 4 ==========
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];
const combined = [...west, ...east];

//Driver Code
console.log('^^^^^^^^^^ No. 4 ^^^^^^^^^^');
console.log(combined);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 4 ==========



// ========== soal 5 ==========
const planet = 'earth';
const view = 'glass';
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

//Driver Code
console.log('^^^^^^^^^^ No. 5 ^^^^^^^^^^');
console.log(before);
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 5 ==========
