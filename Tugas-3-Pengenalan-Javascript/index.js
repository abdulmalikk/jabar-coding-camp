// Abdul Malik
// Tugas 3 JCC - pengenalan javscript


// ========== soal 1 ==========
var pertama = 'saya sangat senang hari ini';
var kedua = 'belajar javascript itu keren';

var editPertama = pertama.substring(0, 18);
var editKedua = kedua.substring(0, 18).toUpperCase();
var hasil1 = editPertama + ' ' + editKedua;
console.log(hasil1);   // saya sangat senang JAVASCRIPT
// ========== soal 1 ==========


// ========== soal 2 ==========
var kataPertama = Number('10');
var kataKedua = Number('2');
var kataKetiga = Number('4');
var kataKeempat = Number('6');

var hasil2 = kataKeempat + (kataKetiga * kataKedua) + kataPertama;
console.log(hasil2);    // 24
// ========== soal 2 ==========


// ========== soal 3 ==========
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(3, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + kataPertama);    // Kata Pertama: wah
console.log('Kata Kedua: ' + kataKedua);    // Kata Kedua: javascript
console.log('Kata Ketiga: ' + kataKetiga);    // Kata Ketiga: itu
console.log('Kata Keempat: ' + kataKeempat);    // Kata Keempat: keren
console.log('Kata Kelima: ' + kataKelima);    // Kata Kelima: sekali
// ========== soal 3 ==========
