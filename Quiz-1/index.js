// Abdul Malik
// JCC - Kuis 1


// ========== soal 1 ==========
// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

var tanggal = 29
var bulan = 2
var tahun = 2020

function next_date(tanggal, bulan, tahun) {
  var tanggalLengkap = new Date(tahun, bulan, tanggal);
  tanggalLengkap.setDate(tanggalLengkap.getDate() + 1);
  var bulanBaru;
  switch (bulanBaru) {
    case 1:
      bulanBaru = 'Januari';
      break;
    case 2:
      bulanBaru = 'Februari';
      break;
    case 3:
      bulanBaru = 'Maret';
      break;
    case 4:
      bulanBaru = 'April';
      break;
    case 5:
      bulanBaru = 'Mei';
      break;
    case 6:
      bulanBaru = 'Juni';
      break;
    case 7:
      bulanBaru = 'Juli';
      break;
    case 8:
      bulanBaru = 'Agustus';
      break;
    case 9:
      bulanBaru = 'September';
      break;
    case 10:
      bulanBaru = 'Oktober';
      break;
    case 11:
      bulanBaru = 'November';
      break;
    case 12:
      bulanBaru = 'Desember';
      break;
    default:
      bulanBaru = '-';
      break;
  }
  return tanggalLengkap;
}

console.log(next_date(tanggal , bulan , tahun) ) // output : 1 Maret 2020
// ========== soal 1 ==========


// ========== soal 2 ==========
var contoh = 'nama saya nama saya';

function jumlah_kata(kalimat) {
  var hasilArr = [];
  var hasilKata = '';
  for (var i = 0; i < kalimat.length; i++) {
    if (kalimat[i] == ' ') {
      hasilArr.push(hasilKata);
      hasilKata = '';
    } else {
      hasilKata += kalimat[i];
    }
  }
  if (hasilKata) hasilArr.push(hasilKata);
  return hasilArr.length;
}

console.log('^^^^^^^^^^ No. 2 ^^^^^^^^^^');
console.log(jumlah_kata(contoh));
console.log('^^^^^^^^^^^^^^^^^^^^^^^^^ \n');
// ========== soal 2 ==========
